#!/usr/bin/env python3.4

import smtplib, os
from contextlib import contextmanager
import argparse

from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

## below is python2 imports
## from email.MIMEBase import MIMEBase
## from email.MIMEMultipart import MIMEMultipart
## from email.MIMEText import MIMEText
## from email.Utils import COMMASPACE, formatdate
## from email import Encoders

import os

FROM_ADDRESS = os.environ['EMAIL_ADDRESS']
EMAIL_PASSWORD = os.environ['EMAIL_PASSWORD']
CC_ADDRESS = os.environ['CC_ADDRESS']
SMTP_SERVER = os.environ['SMTP_SERVER']
SMTP_PORT = os.environ['SMTP_PORT']


def quickie(title, body='', to_addresses=[], files=[]):
    to_addresses.append(CC_ADDRESS)
    message = generate_message(to_addresses, title, text_plain=body, files=files)
    send_email(to_addresses, message)


def generate_message(recipients, subject, text_html=None, text_plain=None, files=[]):
    assert type(files)==list
    if type(recipients)!=list:
        recipients = [recipients]

    msg = MIMEMultipart('alternative')
    msg['From'] = FROM_ADDRESS
    msg['To'] = COMMASPACE.join(recipients)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    if text_plain is not None:
        msg.attach(MIMEText(text_plain, 'plain'))
    if text_html is not None:
        msg.attach(MIMEText(text_html, 'html'))

    for file_path in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(file_path,"rb").read())
        encoders.encode_base64(part)
        part.add_header(
            'Content-Disposition',
            'attachment; filename="%s"' % os.path.basename(file_path)
        )
        msg.attach(part)

    return msg.as_string()


def send_email(recipients, message):
    with smtp_email_server() as server:
        server.sendmail(FROM_ADDRESS, recipients, message)

# the same as a context manager object with an __init__, __entry__, __exit__ functions
@contextmanager
def smtp_email_server():
    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.ehlo()
    server.starttls()
    server.login(FROM_ADDRESS, EMAIL_PASSWORD)
    yield server
    server.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Send an email!')
    parser.add_argument('subject', type=str)
    parser.add_argument('body', type=str, default='')
    parser.add_argument('--to_recipients', default=[CC_ADDRESS], type=str, nargs='+')
    parser.add_argument('--files', default=[], type=str, nargs='+')
    args = parser.parse_args()
    quickie(args.subject, args.body, to_addresses=args.to_recipients, files=args.files)
